#ifndef APARTMENT_H
#define APARTMENT_H


class Apartment
{
private:
    int roomCount;//количество комнат
    float area;//площадь
public:
    Apartment(int RoomCount, float Area);

    int getRoomCount(){return roomCount;}
    float getArea(){return  area;}
};

#endif // APARTMENT_H
