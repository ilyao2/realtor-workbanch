#include "createrentselldialog.h"
#include "ui_createrentselldialog.h"

CreateRentSellDialog::CreateRentSellDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateRentSellDialog)
{
    ui->setupUi(this);
    setModal(true);
}

CreateRentSellDialog::~CreateRentSellDialog()
{
    delete ui;
    if(order != nullptr)delete order;
    for(int i=0; i < apartments.size(); i++)
        if(apartments[i] != nullptr)delete apartments[i];
}

void CreateRentSellDialog::on_Cancal_clicked()
{
    hide();
}

void CreateRentSellDialog::on_create_clicked()
{
    if(ui->spinBox_3->value() > ui->spinBox_4->value())
    {
        ui->error->setText("Неверно установлена стоимость");
        return;
    }
    if(ui->spinBox_2->value() > ui->spinBox_5->value())
    {
        ui->error->setText("Неверно установлено время аренды");
        return;
    }
    Apartment* apartment = new Apartment(ui->spinBox->value(), ui->doubleSpinBox->value());
    apartments.append(apartment);
    order = new RentSellOrder(
                ui->lineEdit->text(),
                ui->spinBox_3->value(),
                ui->spinBox_4->value(),
                ui->spinBox_2->value(),
                ui->spinBox_5->value(),
                apartment);
    setDefault();
    hide();
    emit orderGenerated();
}

RentSellOrder* CreateRentSellDialog::getLastOrder()
{
    RentSellOrder* temp = order;
    order = nullptr;
    setDefault();
    return temp;
}

void CreateRentSellDialog::setDefault()
{
    ui->error->setText("");
    ui->lineEdit->setText("Автор");
    ui->spinBox->setValue(0);
    ui->doubleSpinBox->setValue(0);
    ui->spinBox_3->setValue(0);
    ui->spinBox_4->setValue(0);
    ui->spinBox_2->setValue(0);
    ui->spinBox_5->setValue(0);
}
