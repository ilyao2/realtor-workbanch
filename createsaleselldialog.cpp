#include "createsaleselldialog.h"
#include "ui_createsaleselldialog.h"

CreateSaleSellDialog::CreateSaleSellDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateSaleSellDialog)
{
    ui->setupUi(this);
    setModal(true);
}

CreateSaleSellDialog::~CreateSaleSellDialog()
{
    delete ui;
    if(order != nullptr)delete order;
    for(int i=0; i < apartments.size(); i++)
        if(apartments[i] != nullptr)delete apartments[i];
}

void CreateSaleSellDialog::on_Cancal_clicked()
{
    hide();
}

void CreateSaleSellDialog::on_create_clicked()
{
    if(ui->spinBox_3->value() > ui->spinBox_4->value())
    {
        ui->error->setText("Неверно установлена стоимость");
        return;
    }
    Apartment* apartment = new Apartment(ui->spinBox->value(), ui->doubleSpinBox->value());
    apartments.append(apartment);
    order = new SaleSellOrder(
                ui->lineEdit->text(),
                ui->spinBox_3->value(),
                ui->spinBox_4->value(),
                apartment);
    setDefault();
    hide();
    emit orderGenerated();
}

SaleSellOrder* CreateSaleSellDialog::getLastOrder()
{
    SaleSellOrder* temp = order;
    order = nullptr;
    setDefault();
    return temp;
}

void CreateSaleSellDialog::setDefault()
{
    ui->error->setText("");
    ui->lineEdit->setText("Автор");
    ui->spinBox->setValue(0);
    ui->doubleSpinBox->setValue(0);
    ui->spinBox_3->setValue(0);
    ui->spinBox_4->setValue(0);
}
