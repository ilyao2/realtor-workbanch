#ifndef CREATESALESELLDIALOG_H
#define CREATESALESELLDIALOG_H

#include <QDialog>
#include "order.h"

namespace Ui {
class CreateSaleSellDialog;
}

class CreateSaleSellDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateSaleSellDialog(QWidget *parent = nullptr);
    ~CreateSaleSellDialog();
    SaleSellOrder* getLastOrder();

signals:
    void orderGenerated();
private slots:
    void on_Cancal_clicked();

    void on_create_clicked();

private:
    Ui::CreateSaleSellDialog *ui;
    SaleSellOrder* order;
    QList<Apartment*> apartments;

    void setDefault();
};

#endif // CREATESALESELLDIALOG_H
