#ifndef DEALDIALOG_H
#define DEALDIALOG_H

#include <QDialog>
#include <QListWidget>
#include "order.h"
#include <string>

namespace DEAL_SIDE {
    const bool B_S = false;
    const bool S_B = true;
}

namespace Ui {
class DealDialog;
}

class DealDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DealDialog(QWidget *parent = nullptr);
    ~DealDialog();

    void setDealInfo(SellOrder* order, QList<BuyOrder*>& B_orders); //передать информацию о заказах и подобрать подходящие
    void setDealInfo(BuyOrder* order, QList<SellOrder*>& S_orders);

signals:
    void dealCreated();//сигнал о том, что сделка создана

private slots:
    void on_cancal_clicked();//отказ от создания сделки

    void on_create_clicked();//кнопка создать сделку

    void on_listWidget_currentRowChanged(int currentRow);//при выбори разных элементов списка доступных заказов

private:
    bool Deal_Side = DEAL_SIDE::B_S;//в какую сторону идёт оснвной заказ сделки
    SellOrder* sellOrder;
    BuyOrder* buyOrder;
    QList<QListWidgetItem*> items;

    QList<BuyOrder*> goodB_Orders;//подходящие заказы к основному
    QList<SellOrder*> goodS_Orders;

    BuyOrder* selectedB_Order;//выбранный из списка подходящих
    SellOrder* selectedS_Order;

    Ui::DealDialog *ui;
};

#endif // DEALDIALOG_H
