#include "dealdialog.h"
#include "ui_dealdialog.h"

DealDialog::DealDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DealDialog)
{
    ui->setupUi(this);
}

DealDialog::~DealDialog()
{
    delete ui;
    for(int i=0; i < items.size(); i++)
        if(items[i] != nullptr)delete  items[i];
}

void DealDialog::setDealInfo(BuyOrder* order, QList<SellOrder*>& S_orders)//в соответствии с переданной информацией заполняем диалог
{
    Deal_Side = DEAL_SIDE::B_S;
    buyOrder = order;
    goodS_Orders.clear();
    ui->number->setText(std::to_string(order->getId()).c_str());
    ui->author->setText(order->getAuthorName());
    ui->listWidget->clear();
    QListWidgetItem* item = nullptr;
    for(int i = 0; i<S_orders.size();i++)
    {
        if(order->isDeal(S_orders[i]))
        {
            goodS_Orders.append(S_orders[i]);
            item = new QListWidgetItem;
            QString str = "Заказ №";
            str += std::to_string(S_orders[i]->getId()).c_str();
            str += ": автор: ";
            str += S_orders[i]->getAuthorName();
            str += ", комнат: ";
            str += std::to_string(S_orders[i]->getRoomCount()).c_str();
            str += ", площадь: ";
            str += std::to_string(S_orders[i]->getArea()).c_str();
            str += ", цена: ";
            str += std::to_string(S_orders[i]->getMinPrice()).c_str();
            str += "-";
            str += std::to_string(S_orders[i]->getMaxPrice()).c_str();
            item->setText(str);
            ui->listWidget->addItem(item);
        }
    }
}
void DealDialog::setDealInfo(SellOrder* order, QList<BuyOrder*>& B_orders)
{
    Deal_Side = DEAL_SIDE::S_B;
    sellOrder = order;
    goodB_Orders.clear();
    ui->number->setText(std::to_string(order->getId()).c_str());
    ui->author->setText(order->getAuthorName());

    ui->listWidget->clear();

    QListWidgetItem* item = nullptr;
    for(int i = 0; i<B_orders.size();i++)
    {
        if(order->isDeal(B_orders[i]))
        {
            goodB_Orders.append(B_orders[i]);
            item = new QListWidgetItem;
            QString str = "Заказ №";
            str += std::to_string(B_orders[i]->getId()).c_str();
            str += ": автор: ";
            str += B_orders[i]->getAuthorName();
            str += ", цена: ";
            str += std::to_string(B_orders[i]->getMinPrice()).c_str();
            str += "-";
            str += std::to_string(B_orders[i]->getMaxPrice()).c_str();
            item->setText(str);
            ui->listWidget->addItem(item);
        }
    }
}

void DealDialog::on_cancal_clicked()//при отмене просто скрываем его
{
    hide();
}

void DealDialog::on_create_clicked() //если нажата кнопка проверить выбран ли элемент и создать с ним сделку
{

    if(Deal_Side == DEAL_SIDE::B_S)
    {
        if(selectedS_Order != nullptr)
        {
            buyOrder->Deal(selectedS_Order);
            emit dealCreated();
            hide();
        }
    }
    else
    {
        if(selectedB_Order != nullptr)
        {
            sellOrder->Deal(selectedB_Order);
            emit dealCreated();
            hide();
        }
    }
}

void DealDialog::on_listWidget_currentRowChanged(int currentRow) //при выборе различных предложенных вариантов делать выбранные заказы текущими и отображать их цену
{
    if(currentRow == -1)
    {
        selectedB_Order = nullptr;
        selectedS_Order = nullptr;
        return;
    }
    if(Deal_Side == DEAL_SIDE::B_S)
    {
        selectedS_Order = goodS_Orders[currentRow];
        ui->price->setText(std::to_string(buyOrder->DealPrice(selectedS_Order)).c_str());
    }
    else
    {
        selectedB_Order = goodB_Orders[currentRow];
        ui->price->setText(std::to_string(buyOrder->DealPrice(selectedS_Order)).c_str());
    }
}
