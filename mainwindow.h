#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "createsalebuydialog.h"
#include "createrentbuydialog.h"
#include "createsaleselldialog.h"
#include "createrentselldialog.h"
#include "dealdialog.h"
#include "order.h"
#include <QTableWidgetItem>
#include <string>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots://слоты
    void showCreateSaleBuy();//показать диалог создание заказа на покупку
    void createSaleBuy(); //создать заказ на покупку

    void showCreateRentBuy();//показать диалог создание заказа на аренду
    void createRentBuy();//создать заказ на аренду

    void showCreateSaleSell();//показать диалог создание заказа на продажу
    void createSaleSell();//создать заказ на продажу

    void showCreateRentSell();//показать диалог создание заказа на сдачу в аренду
    void createRentSell();//создать заказ на сдачу в аренду

    void showSaleBuy();//показать таблицу заказов на покупку
    void showRentBuy();//на аренду
    void showSaleSell();//на продажу
    void showRentSell();//на сдачу в аренду


    void updateAll()//обновить все таблицы
    {
        updateRentBuy();updateSaleBuy();updateRentSell();updateSaleSell();
    }


    void on_createDealButton_clicked();

private:
    //обновить таблицы
    void updateSaleBuy();
    void updateRentBuy();
    void updateSaleSell();
    void updateRentSell();
    Ui::MainWindow *ui;
    //диалоги создания заказа
    CreateSaleBuyDialog* createSaleBuyDialog = new CreateSaleBuyDialog(this);
    CreateRentBuyDialog* createRentBuyDialog = new CreateRentBuyDialog(this);
    CreateSaleSellDialog* createSaleSellDialog = new CreateSaleSellDialog(this);
    CreateRentSellDialog* createRentSellDialog = new CreateRentSellDialog(this);
    //диалог создания сделки
    DealDialog* dealDialog = new DealDialog(this);
    QTableWidget* currentTable = nullptr;//текущая таблица
    QList<Order*> Orders;//все заказы
    QList<BuyOrder*> buyOrders;//заказы покупателей
    QList<SellOrder*> sellOrders;//заказы продающих
    QList<QTableWidgetItem*> items;//список элементов всех таблиц

};
#endif // MAINWINDOW_H
