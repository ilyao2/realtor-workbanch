#include "createrentbuydialog.h"
#include "ui_createrentbuydialog.h"

CreateRentBuyDialog::CreateRentBuyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateRentBuyDialog)
{
    ui->setupUi(this);
    setModal(true);
}

CreateRentBuyDialog::~CreateRentBuyDialog()
{
    delete ui;
    if(order != nullptr)delete order;
}

void CreateRentBuyDialog::on_Cancal_clicked()
{
    hide();
}

void CreateRentBuyDialog::on_create_clicked()
{
    if(ui->spinBox_3->value() > ui->spinBox_4->value())
    {
        ui->error->setText("Неверно установлена стоимость");
        return;
    }
    if(ui->spinBox->value() > ui->spinBox_2->value())
    {
        ui->error->setText("Неверно установлено количество комнат");
        return;
    }
    if(ui->doubleSpinBox->value() > ui->doubleSpinBox_2->value())
    {
        ui->error->setText("Неверно установлена площадь");
        return;
    }
    if(ui->spinBox_5->value() > ui->spinBox_6->value())
    {
        ui->error->setText("Неверно установлено время аренды");
        return;
    }
    order = new RentBuyOrder(
                ui->lineEdit->text(),
                ui->spinBox_3->value(),
                ui->spinBox_4->value(),
                ui->spinBox->value(),
                ui->spinBox_2->value(),
                ui->doubleSpinBox->value(),
                ui->doubleSpinBox_2->value(),
                ui->spinBox_5->value(),
                ui->spinBox_6->value());
    setDefault();
    hide();
    emit orderGenerated();
}

RentBuyOrder* CreateRentBuyDialog::getLastOrder()
{
    RentBuyOrder* temp = order;
    order = nullptr;
    setDefault();
    return temp;
}

void CreateRentBuyDialog::setDefault()
{
    ui->error->setText("");
    ui->lineEdit->setText("Автор");
    ui->spinBox->setValue(0);
    ui->spinBox_2->setValue(0);
    ui->doubleSpinBox->setValue(0);
    ui->doubleSpinBox_2->setValue(0);
    ui->spinBox_3->setValue(0);
    ui->spinBox_4->setValue(0);
    ui->spinBox_5->setValue(0);
    ui->spinBox_6->setValue(0);
}
