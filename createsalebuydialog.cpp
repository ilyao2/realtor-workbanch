#include "createsalebuydialog.h"
#include "ui_createsalebuydialog.h"

CreateSaleBuyDialog::CreateSaleBuyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateSaleBuyDialog)
{
    ui->setupUi(this);
    setModal(true);
}

CreateSaleBuyDialog::~CreateSaleBuyDialog()
{
    delete ui;
    if(order != nullptr)delete order;
}

void CreateSaleBuyDialog::on_Cancal_clicked()
{
    hide();
}

void CreateSaleBuyDialog::on_create_clicked()
{
    if(ui->spinBox_3->value() > ui->spinBox_4->value())
    {
        ui->error->setText("Неверно установлена стоимость");
        return;
    }
    if(ui->spinBox->value() > ui->spinBox_2->value())
    {
        ui->error->setText("Неверно установлено количество комнат");
        return;
    }
    if(ui->doubleSpinBox->value() > ui->doubleSpinBox_2->value())
    {
        ui->error->setText("Неверно установлена площадь");
        return;
    }
    order = new SaleBuyOrder(
                ui->lineEdit->text(),
                ui->spinBox_3->value(),
                ui->spinBox_4->value(),
                ui->spinBox->value(),
                ui->spinBox_2->value(),
                ui->doubleSpinBox->value(),
                ui->doubleSpinBox_2->value());
    setDefault();
    hide();
    emit orderGenerated();
}

SaleBuyOrder* CreateSaleBuyDialog::getLastOrder()
{
    SaleBuyOrder* temp = order;
    order = nullptr;
    setDefault();
    return temp;
}

void CreateSaleBuyDialog::setDefault()
{
    ui->error->setText("");
    ui->lineEdit->setText("Автор");
    ui->spinBox->setValue(0);
    ui->spinBox_2->setValue(0);
    ui->doubleSpinBox->setValue(0);
    ui->doubleSpinBox_2->setValue(0);
    ui->spinBox_3->setValue(0);
    ui->spinBox_4->setValue(0);
}
