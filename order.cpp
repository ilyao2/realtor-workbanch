#include "order.h"

unsigned int Order::idCounter = 0;
Order::Order(QString AuthorName, int MinPrice, int MaxPrice)
{
    id = idCounter++; //назначаем идентификатор
    //устанавливаем начальные значения полей
    authorName = AuthorName;
    minPrice = MinPrice;
    maxPrice = MaxPrice;
    status = ORDER_STATUS::OPEN;
}

BuyOrder::BuyOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinRoomCount, int MaxRoomCount, float MinArea, float MaxArea) :
    Order(AuthorName, MinPrice, MaxPrice)
{
    //начальные значения полей
    minRoomCount = MinRoomCount;
    maxRoomCount = MaxRoomCount;
    minArea = MinArea;
    maxArea = MaxArea;
}

SellOrder::SellOrder(QString AuthorName, int MinPrice, int MaxPrice, Apartment* apartment) :
    Order(AuthorName, MinPrice, MaxPrice)
{
    //начальные значения полей
    this->apartment = apartment;
}

RentOrder::RentOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinMonth, int MaxMonth) :
    Order(AuthorName, MinPrice, MaxPrice)
{
    // начальные значения
    minMonth = MinMonth;
    maxMonth = MaxMonth;
}

SaleOrder::SaleOrder(QString AuthorName, int MinPrice, int MaxPrice) :
    Order(AuthorName, MinPrice, MaxPrice)
{
//У заказа связанного с куплей/продажей нет доп полей
    //определяется конструктором базового класса
}
/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

bool RentBuyOrder::isDeal(SellOrder *order)
{
    if(getStatus() == ORDER_STATUS::CLOSE || order->getStatus() == ORDER_STATUS::CLOSE) //проверка на статусы сделок
        return 0;
    if(typeid (*order) != typeid (RentSellOrder)) // проверка на соответствие подходящему типу (для класса свзянного с  арендой, нужна пара с арендой)
        return 0;
    if(getMinPrice() > order->getMaxPrice() || getMaxPrice() < order->getMinPrice())//проверка на то, что стороны смогут сойтись в цене
        return 0;

    RentSellOrder* temp = static_cast<RentSellOrder*>(order); // статическое приведение (мы проверили на соответствие типу, можно считать безопасным)
    if(temp->getRoomCount() < getMinRoomCount() || temp->getRoomCount() > getMaxRoomCount()) //проверка на то что покупателя устраивает кол-во комнат
        return 0;
    if(temp->getArea() < getMinArea() || temp->getArea() > getMaxArea())//проверка на то, что покупателя устраивает площадь
        return 0;


    if(getMinMonth() > temp->getMaxMonth() || getMaxMonth() < temp->getMinMonth())//проверка на то, чтостороны смогут сойтись во времени аренды
        return 0;
    return 1;
}
int RentBuyOrder::DealPrice(SellOrder *order)
{
    if(!isDeal(order)) return 0; //если не смогут сойтись 0
    RentSellOrder* temp = static_cast<RentSellOrder*>(order);//опасное приведение, нужно проверять косвенно через isDeal
    int monthCount = AVGintInterval(getMinMonth(), getMaxMonth(), temp->getMinMonth(), temp->getMaxMonth()); // на сколько месяцев будет аренда

    int AVGPrice = AVGintInterval(getMinPrice(),getMaxPrice(),temp->getMinPrice(), temp->getMaxPrice())*monthCount; // на какую сумму выйдет сделка

    return AVGPrice-((( -50/( (monthCount*1.0)/10+1 )) + 50 )/100)*(AVGPrice*1.0); // сумма сделки с учётом скидки
}
int RentBuyOrder::Deal(SellOrder *order)
{
    int Price = DealPrice(order);//пролучение суммы сделки

    //запись в файл
    QString fileName = "Deal (";
    fileName += std::to_string(order->getId()).c_str();
    fileName += "-";
    fileName += std::to_string(getId()).c_str();
    fileName += ").txt";
    std::ofstream fout(fileName.toStdString());
    fout << "Продавец: " << order->getAuthorName().toStdString() << "\n";
    fout << "Покупатель: " << getAuthorName().toStdString() << "\n";
    fout << "    Объект:\n        Комнат: " << order->getRoomCount() << "\n        Площадь: " << order->getArea() << "\n";
    fout << "____________________________________\n";
    RentSellOrder* temp = static_cast<RentSellOrder*>(order);
    fout << "Цена за " << AVGintInterval(getMinMonth(), getMaxMonth(), temp->getMinMonth(), temp->getMaxMonth()) << " месяцев: " << Price << "\n";

    if(isDeal(order))//если сделка возможна, изменить статусы сделок
    {
        changeStatus();
        order->changeStatus();
    }
    return Price;//вернуть итоговую сумму
}

bool RentSellOrder::isDeal(BuyOrder *order)//аналогично предыдущему
{
    if(getStatus() == ORDER_STATUS::CLOSE || order->getStatus() == ORDER_STATUS::CLOSE)
        return 0;
    if(typeid (*order) != typeid (RentBuyOrder))
        return 0;
    if(getMinPrice() > order->getMaxPrice() || getMaxPrice() < order->getMinPrice())
        return 0;

    RentBuyOrder* temp = static_cast<RentBuyOrder*>(order);
    if(getRoomCount() < temp->getMinRoomCount() || getRoomCount() > temp->getMaxRoomCount())
        return 0;
    if(getArea() < temp->getMinArea() || getArea() > temp->getMaxArea())
        return 0;

    if(getMinMonth() > temp->getMaxMonth() || getMaxMonth() < temp->getMinMonth())
        return 0;

    return 1;
}
int RentSellOrder::DealPrice(BuyOrder *order)
{
    if(!isDeal(order)) return 0;
    RentBuyOrder* temp = static_cast<RentBuyOrder*>(order);
    int monthCount = AVGintInterval(getMinMonth(), getMaxMonth(), temp->getMinMonth(), temp->getMaxMonth());
    int AVGPrice = AVGintInterval(getMinPrice(),getMaxPrice(),temp->getMinPrice(), temp->getMaxPrice())*monthCount;

    return AVGPrice-((( -50/( (monthCount*1.0)/10+1 )) + 50 )/100)*(AVGPrice*1.0); // сумма сделки с учётом скидки
}
int RentSellOrder::Deal(BuyOrder *order)
{

    int Price = DealPrice(order);

    QString fileName = "Deal (";
    fileName += std::to_string(getId()).c_str();
    fileName += "-";
    fileName += std::to_string(order->getId()).c_str();
    fileName += ").txt";
    std::ofstream fout(fileName.toStdString());
    fout << "Продавец: " << getAuthorName().toStdString() << "\n";
    fout << "Покупатель: " << order->getAuthorName().toStdString() << "\n";
    fout << "    Объект:\n        Комнат: " << getRoomCount() << "\n        Площадь: " << getArea() << "\n";
    fout << "____________________________________\n";
    RentBuyOrder* temp = static_cast<RentBuyOrder*>(order);
    fout << "Цена за " << AVGintInterval(getMinMonth(), getMaxMonth(), temp->getMinMonth(), temp->getMaxMonth()) << " месяцев: " << Price << "\n";

    if(isDeal(order))
    {
        changeStatus();
        order->changeStatus();
    }
    return Price;
}
bool SaleBuyOrder::isDeal(SellOrder *order)//аналогично предыдущему
{
    if(getStatus() == ORDER_STATUS::CLOSE || order->getStatus() == ORDER_STATUS::CLOSE)
        return 0;
    if(typeid (*order) != typeid (SaleSellOrder))
        return 0;
    if(getMinPrice() > order->getMaxPrice() || getMaxPrice() < order->getMinPrice())
        return 0;

    SaleSellOrder* temp = static_cast<SaleSellOrder*>(order);
    if(temp->getRoomCount() < getMinRoomCount() || temp->getRoomCount() > getMaxRoomCount())
        return 0;
    if(temp->getArea() < getMinArea() || temp->getArea() > getMaxArea())
        return 0;
    return 1;
}
int SaleBuyOrder::DealPrice(SellOrder *order)
{
    if(!isDeal(order))return 0;
    SaleSellOrder* temp = static_cast<SaleSellOrder*>(order);
    return AVGintInterval(getMinPrice(),getMaxPrice(),temp->getMinPrice(), temp->getMaxPrice());
}
int SaleBuyOrder::Deal(SellOrder *order)
{
    int Price = DealPrice(order);

    QString fileName = "Deal (";
    fileName += std::to_string(order->getId()).c_str();
    fileName += "-";
    fileName += std::to_string(getId()).c_str();
    fileName += ").txt";
    std::ofstream fout(fileName.toStdString());
    fout << "Продавец: " << order->getAuthorName().toStdString() << "\n";
    fout << "Покупатель: " << getAuthorName().toStdString() << "\n";
    fout << "    Объект:\n        Комнат: " << order->getRoomCount() << "\n        Площадь: " << order->getArea() << "\n";
    fout << "____________________________________\n";
    fout << "Цена: " << Price << "\n";

    if(isDeal(order))
    {
        changeStatus();
        order->changeStatus();
    }
    return Price;
}

bool SaleSellOrder::isDeal(BuyOrder *order)//аналагично предыдущему
{
    if(getStatus() == ORDER_STATUS::CLOSE || order->getStatus() == ORDER_STATUS::CLOSE)
        return 0;
    if(typeid (*order) != typeid (SaleBuyOrder))
        return 0;
    if(getMinPrice() > order->getMaxPrice() || getMaxPrice() < order->getMinPrice())
        return 0;

    SaleBuyOrder* temp = static_cast<SaleBuyOrder*>(order);
    if(getRoomCount() < temp->getMinRoomCount() || getRoomCount() > temp->getMaxRoomCount())
        return 0;
    if(getArea() < temp->getMinArea() || getArea() > temp->getMaxArea())
        return 0;
    return 1;
}
int SaleSellOrder::DealPrice(BuyOrder *order)
{
    if(!isDeal(order))return 0;
    SaleBuyOrder* temp = static_cast<SaleBuyOrder*>(order);
    return AVGintInterval(getMinPrice(),getMaxPrice(),temp->getMinPrice(), temp->getMaxPrice());
}
int SaleSellOrder::Deal(BuyOrder *order)
{
    int Price = DealPrice(order);

    QString fileName = "Deal (";
    fileName += std::to_string(getId()).c_str();
    fileName += "-";
    fileName += std::to_string(order->getId()).c_str();
    fileName += ").txt";
    std::ofstream fout(fileName.toStdString());
    fout << "Продавец: " << getAuthorName().toStdString() << "\n";
    fout << "Покупатель: " << order->getAuthorName().toStdString() << "\n";
    fout << "    Объект:\n        Комнат: " << getRoomCount() << "\n        Площадь: " << getArea() << "\n";
    fout << "____________________________________\n";
    fout << "Цена: " << Price << "\n";

    if(isDeal(order))
    {
        changeStatus();
        order->changeStatus();
    }
    return Price;
}

int Order::AVGintInterval(int x1, int x2, int x3, int x4) // Среднее значение пересекающегося интервала
{

    int Mas[4] = {x1, x2, x3, x4}; //сортирую
    for (int i = 0; i < 4 - 1; i++) {
        for (int j = 0; j < 4 - i - 1; j++) {
            if (Mas[j] > Mas[j + 1]) {
                int temp = Mas[j];
                Mas[j] = Mas[j + 1];
                Mas[j + 1] = temp;
            }
        }
    }
    return (Mas[1]+Mas[2])/2; //беру среднее значение между двумя центральными
}
