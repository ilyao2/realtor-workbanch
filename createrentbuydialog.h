#ifndef CREATERENTBUYDIALOG_H
#define CREATERENTBUYDIALOG_H

#include <QDialog>
#include "order.h"

namespace Ui {
class CreateRentBuyDialog;
}

class CreateRentBuyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateRentBuyDialog(QWidget *parent = nullptr);
    ~CreateRentBuyDialog();
    RentBuyOrder* getLastOrder();

signals:
    void orderGenerated();
private slots:
    void on_Cancal_clicked();

    void on_create_clicked();

private:
    Ui::CreateRentBuyDialog *ui;
    RentBuyOrder* order;

    void setDefault();
};
#endif // CREATERENTBUYDIALOG_H
