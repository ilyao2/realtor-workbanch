#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //устанавливаем текующую таблицу
    currentTable = ui->tableSaleBuy;
    ui->tableRentBuy->hide();
    ui->tableRentSell->hide();
    ui->tableSaleSell->hide();
    //подключаем сигналы кнопок меню к слотам показа диалогов
    //подключаем сигналы диалогов о том, что все параметры введены к слотам создания заказа
    connect(ui->action_CreateSaleBuy, SIGNAL(triggered()), this, SLOT(showCreateSaleBuy()));
    connect(createSaleBuyDialog, SIGNAL(orderGenerated()), this, SLOT(createSaleBuy()));

    connect(ui->action_CreateRentBuy, SIGNAL(triggered()), this, SLOT(showCreateRentBuy()));
    connect(createRentBuyDialog, SIGNAL(orderGenerated()), this, SLOT(createRentBuy()));

    connect(ui->action_CreateSaleSell, SIGNAL(triggered()), this, SLOT(showCreateSaleSell()));
    connect(createSaleSellDialog, SIGNAL(orderGenerated()), this, SLOT(createSaleSell()));

    connect(ui->action_CreateRentSell, SIGNAL(triggered()), this, SLOT(showCreateRentSell()));
    connect(createRentSellDialog, SIGNAL(orderGenerated()), this, SLOT(createRentSell()));

    //подключаем сигналы кнопок меню к слотам поеказа таблиц
    connect(ui->action_ShowSaleBuy, SIGNAL(triggered()), this, SLOT(showSaleBuy()));
    connect(ui->action_ShowRentBuy, SIGNAL(triggered()), this, SLOT(showRentBuy()));
    connect(ui->action_ShowSaleSell, SIGNAL(triggered()), this, SLOT(showSaleSell()));
    connect(ui->action_ShowRentSell, SIGNAL(triggered()), this, SLOT(showRentSell()));

    //подключаем сигнал совершения сделки к слоту обновления всех таблиц
    connect(dealDialog, SIGNAL(dealCreated()), this, SLOT(updateAll()));
}

MainWindow::~MainWindow()
{
    //освобождаем всю выделенную память
    delete ui;
    delete createSaleBuyDialog;
    for(int i =0 ; i < Orders.size(); i++)
        if(Orders[i] != nullptr) delete Orders[i];

    for(int i =0 ; i < items.size(); i++)
        if(items[i] != nullptr) delete items[i];
}

//показываем диалоги
void MainWindow::showCreateSaleBuy()
{
    createSaleBuyDialog->show();
}
void MainWindow::showCreateRentBuy()
{
    createRentBuyDialog->show();
}
void MainWindow::showCreateSaleSell()
{
    createSaleSellDialog->show();
}
void MainWindow::showCreateRentSell()
{
    createRentSellDialog->show();
}

//перезаполняем таблицы по спискам заказов
void MainWindow::updateSaleBuy()
{
    ui->tableSaleBuy->setRowCount(0);
    for(int i =0; i < buyOrders.size(); i++)
    {
        BuyOrder* temp = buyOrders[i];
        if(typeid (*temp) != typeid (SaleBuyOrder)) continue;
        SaleBuyOrder* order = static_cast<SaleBuyOrder*>(temp);
        ui->tableSaleBuy->insertRow(ui->tableSaleBuy->rowCount());
        QTableWidgetItem* item1 = new QTableWidgetItem( std::to_string(order->getId()).c_str());
        QTableWidgetItem* item2 = new QTableWidgetItem(order->getAuthorName());
        QTableWidgetItem* item3 = new QTableWidgetItem(std::to_string(order->getMinRoomCount()).c_str());
        QTableWidgetItem* item4 = new QTableWidgetItem(std::to_string(order->getMaxRoomCount()).c_str());
        QTableWidgetItem* item5 = new QTableWidgetItem(std::to_string(order->getMinArea()).c_str());
        QTableWidgetItem* item6 = new QTableWidgetItem(std::to_string(order->getMaxArea()).c_str());
        QTableWidgetItem* item7 = new QTableWidgetItem(std::to_string(order->getMinPrice()).c_str());
        QTableWidgetItem* item8 = new QTableWidgetItem(std::to_string(order->getMaxPrice()).c_str());

        QTableWidgetItem* item9 = new QTableWidgetItem();
        if(order->getStatus() == ORDER_STATUS::OPEN)
            item9->setText("Открыта");
        else
            item9->setText("Закрыта");
        items.append(item1);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 0 , item1);
        items.append(item2);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 1 , item2);
        items.append(item3);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 2 , item3);
        items.append(item4);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 3 , item4);
        items.append(item5);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 4 , item5);
        items.append(item6);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 5 , item6);
        items.append(item7);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 6 , item7);
        items.append(item8);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 7 , item8);
        items.append(item9);   ui->tableSaleBuy->setItem(ui->tableSaleBuy->rowCount()-1, 8 , item9);
    }
}
void MainWindow::updateRentBuy()
{

    ui->tableRentBuy->setRowCount(0);
    for(int i =0; i < buyOrders.size(); i++)
    {
        BuyOrder* temp = buyOrders[i];
        if(typeid (*temp) != typeid (RentBuyOrder)) continue;
        RentBuyOrder* order = static_cast<RentBuyOrder*>(temp);
        ui->tableRentBuy->insertRow(ui->tableRentBuy->rowCount());
        QTableWidgetItem* item1 = new QTableWidgetItem( std::to_string(order->getId()).c_str());
        QTableWidgetItem* item2 = new QTableWidgetItem(order->getAuthorName());
        QTableWidgetItem* item3 = new QTableWidgetItem(std::to_string(order->getMinRoomCount()).c_str());
        QTableWidgetItem* item4 = new QTableWidgetItem(std::to_string(order->getMaxRoomCount()).c_str());
        QTableWidgetItem* item5 = new QTableWidgetItem(std::to_string(order->getMinArea()).c_str());
        QTableWidgetItem* item6 = new QTableWidgetItem(std::to_string(order->getMaxArea()).c_str());
        QTableWidgetItem* item7 = new QTableWidgetItem(std::to_string(order->getMinMonth()).c_str());
        QTableWidgetItem* item8 = new QTableWidgetItem(std::to_string(order->getMaxMonth()).c_str());
        QTableWidgetItem* item9 = new QTableWidgetItem(std::to_string(order->getMinPrice()).c_str());
        QTableWidgetItem* item10 = new QTableWidgetItem(std::to_string(order->getMaxPrice()).c_str());

        QTableWidgetItem* item11 = new QTableWidgetItem();
        if(order->getStatus() == ORDER_STATUS::OPEN)
            item11->setText("Открыта");
        else
            item11->setText("Закрыта");
        items.append(item1);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 0 , item1);
        items.append(item2);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 1 , item2);
        items.append(item3);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 2 , item3);
        items.append(item4);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 3 , item4);
        items.append(item5);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 4 , item5);
        items.append(item6);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 5 , item6);
        items.append(item7);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 6 , item7);
        items.append(item8);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 7 , item8);
        items.append(item9);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 8 , item9);
        items.append(item10);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 9 , item10);
        items.append(item11);   ui->tableRentBuy->setItem(ui->tableRentBuy->rowCount()-1, 10 , item11);
    }
}
void MainWindow::updateSaleSell()
{

    ui->tableSaleSell->setRowCount(0);
    for(int i =0; i < sellOrders.size(); i++)
    {
        SellOrder* temp = sellOrders[i];
        if(typeid (*temp) != typeid (SaleSellOrder)) continue;
        SaleSellOrder* order = static_cast<SaleSellOrder*>(temp);
        ui->tableSaleSell->insertRow(ui->tableSaleSell->rowCount());
        QTableWidgetItem* item1 = new QTableWidgetItem( std::to_string(order->getId()).c_str());
        QTableWidgetItem* item2 = new QTableWidgetItem(order->getAuthorName());
        QTableWidgetItem* item3 = new QTableWidgetItem(std::to_string(order->getRoomCount()).c_str());
        QTableWidgetItem* item4 = new QTableWidgetItem(std::to_string(order->getArea()).c_str());
        QTableWidgetItem* item5 = new QTableWidgetItem(std::to_string(order->getMinPrice()).c_str());
        QTableWidgetItem* item6 = new QTableWidgetItem(std::to_string(order->getMaxPrice()).c_str());
        QTableWidgetItem* item7 = new QTableWidgetItem();
        if(order->getStatus() == ORDER_STATUS::OPEN)
            item7->setText("Открыта");
        else
            item7->setText("Закрыта");
        items.append(item1);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 0 , item1);
        items.append(item2);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 1 , item2);
        items.append(item3);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 2 , item3);
        items.append(item4);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 3 , item4);
        items.append(item5);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 4 , item5);
        items.append(item6);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 5 , item6);
        items.append(item6);   ui->tableSaleSell->setItem(ui->tableSaleSell->rowCount()-1, 6 , item7);
    }
}
void MainWindow::updateRentSell()
{
    ui->tableRentSell->setRowCount(0);
    for(int i =0; i < sellOrders.size(); i++)
    {
        SellOrder* temp = sellOrders[i];
        if(typeid (*temp) != typeid (RentSellOrder)) continue;
        RentSellOrder* order = static_cast<RentSellOrder*>(temp);
        ui->tableRentSell->insertRow(ui->tableRentSell->rowCount());
        QTableWidgetItem* item1 = new QTableWidgetItem( std::to_string(order->getId()).c_str());
        QTableWidgetItem* item2 = new QTableWidgetItem(order->getAuthorName());
        QTableWidgetItem* item3 = new QTableWidgetItem(std::to_string(order->getRoomCount()).c_str());
        QTableWidgetItem* item4 = new QTableWidgetItem(std::to_string(order->getArea()).c_str());
        QTableWidgetItem* item5 = new QTableWidgetItem(std::to_string(order->getMinMonth()).c_str());
        QTableWidgetItem* item6 = new QTableWidgetItem(std::to_string(order->getMaxMonth()).c_str());
        QTableWidgetItem* item7 = new QTableWidgetItem(std::to_string(order->getMinPrice()).c_str());
        QTableWidgetItem* item8 = new QTableWidgetItem(std::to_string(order->getMaxPrice()).c_str());
        QTableWidgetItem* item9 = new QTableWidgetItem();
        if(order->getStatus() == ORDER_STATUS::OPEN)
            item9->setText("Открыта");
        else
            item9->setText("Закрыта");
        items.append(item1);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 0 , item1);
        items.append(item2);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 1 , item2);
        items.append(item3);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 2 , item3);
        items.append(item4);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 3 , item4);
        items.append(item5);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 4 , item5);
        items.append(item6);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 5 , item6);
        items.append(item7);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 6 , item7);
        items.append(item8);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 7 , item8);
        items.append(item9);   ui->tableRentSell->setItem(ui->tableRentSell->rowCount()-1, 8 , item9);
    }
}
//создание заказов
void MainWindow::createSaleBuy()
{
    SaleBuyOrder* order;
    order = createSaleBuyDialog->getLastOrder();//получение последнего заказа созданного диалогом
    buyOrders.append( order );
    Orders.append( order );
    updateSaleBuy();
}

void MainWindow::createRentBuy()
{
    RentBuyOrder* order;
    order = createRentBuyDialog->getLastOrder();
    buyOrders.append( order );
    Orders.append( order );
    updateRentBuy();
}

void MainWindow::createSaleSell()
{
    SaleSellOrder* order;
    order = createSaleSellDialog->getLastOrder();
    sellOrders.append( order );
    Orders.append( order );
    updateSaleSell();
}

void MainWindow::createRentSell()
{
    RentSellOrder* order;
    order = createRentSellDialog->getLastOrder();
    sellOrders.append( order );
    Orders.append( order );
    updateRentSell();
}

//меняем текущую таблицу
void MainWindow::showSaleBuy()
{
    currentTable->hide();
    ui->tableSaleBuy->show();
    currentTable = ui->tableSaleBuy;
}
void MainWindow::showRentBuy()
{
    currentTable->hide();
    ui->tableRentBuy->show();
    currentTable = ui->tableRentBuy;
}
void MainWindow::showSaleSell()
{
    currentTable->hide();
    ui->tableSaleSell->show();
    currentTable = ui->tableSaleSell;
}
void MainWindow::showRentSell()
{
    currentTable->hide();
    ui->tableRentSell->show();
    currentTable = ui->tableRentSell;
}
//при попытке создать сделку
void MainWindow::on_createDealButton_clicked()
{
    if(currentTable == nullptr || currentTable->selectedItems().isEmpty())//проверяем выбран ли элемент таблицы
    {
        ui->statusbar->showMessage("Выберите элемнт одной из таблиц");
        return;
    }
    unsigned int orderID = (currentTable->item(currentTable->selectedItems().first()->row(), 0)->text()).toInt();//находи в списке выбранный заказ по id
    if(currentTable == ui->tableSaleBuy)
    {
        BuyOrder* temp = nullptr;
        for(int i = 0; i < buyOrders.size(); i++)
            if(buyOrders[i]->getId() == orderID)
            {
                temp = buyOrders[i];
                break;
            }
        dealDialog->setDealInfo(temp, sellOrders);//в соответствии с типом передаём информацию в диалог создания сделки
    }
    else if(currentTable == ui->tableRentBuy)
    {
        BuyOrder* temp = nullptr;
        for(int i = 0; i < buyOrders.size(); i++)
            if(buyOrders[i]->getId() == orderID)
            {
                temp = buyOrders[i];
                break;
            }

        dealDialog->setDealInfo(temp, sellOrders);
    }
    else if (currentTable == ui->tableSaleSell)
    {
        SellOrder* temp = nullptr;
        for(int i = 0; i < sellOrders.size(); i++)
            if(sellOrders[i]->getId() == orderID)
            {
                temp = sellOrders[i];
                break;
            }

        dealDialog->setDealInfo(temp, buyOrders);
    }
    else if (currentTable == ui->tableRentSell)
    {
        SellOrder* temp = nullptr;
        for(int i = 0; i < sellOrders.size(); i++)
            if(sellOrders[i]->getId() == orderID)
            {
                temp = sellOrders[i];
                break;
            }

        dealDialog->setDealInfo(temp, buyOrders);
    }
    dealDialog->show();//показываем диалог сделки
}
