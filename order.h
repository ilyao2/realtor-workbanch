#ifndef ORDER_H
#define ORDER_H
#include <QString>
#include "apartment.h"
#include <fstream>
#include <string>


namespace ORDER_STATUS
{
    const bool OPEN = false;
    const bool CLOSE = true;
}

class Order
{
private:
    unsigned int id; // Уникальный идентификатор
    QString authorName; // Имя автора заказа
    int minPrice; // Нижни порог согласия в цене
    int maxPrice; // Верхний порог согласия в цене
    bool status; //  Статус заказа(открыт, закрыт)
    static unsigned int idCounter;//переменная для рассчёта идентификатора
protected:
    int AVGintInterval(int x1, int x2, int x3, int x4); // находит среднее значение на пересечении двух интервалов
public:
    Order(QString AuthorName, int MinPrice, int MaxPrice);
    ~Order(){};
    unsigned int getId(){return id;}
    QString getAuthorName(){return authorName;}
    bool getStatus(){return status;}
    int getMinPrice(){return minPrice;}
    int getMaxPrice(){return maxPrice;}
    void changeStatus(){status = !status;}
};
class SellOrder;
class BuyOrder;
class BuyOrder : virtual public Order // Класс заказа для покупающей стороны, виртуально наследует заказ
{                                     // Виртуально, чтобы при дальнейшем множественном наследовании не создавалась копия объекта заказа
    int minRoomCount; //Минимальный порог согласия на кол-во комнат
    int maxRoomCount; //Максимальный
    float minArea;  //Минимальный порог согласия на площадь
    float maxArea;  //Максимальный
public:
    BuyOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinRoomCount, int MaxRoomCount, float MinArea, float MaxArea);
    virtual ~BuyOrder(){};

    //Чистые виртуальные методы, говорят о том, что класс абстрактный и должны быть перегружены
    virtual bool isDeal(SellOrder* order) = 0; //Возможна ли сделка
    virtual int DealPrice(SellOrder* order) = 0; //На какую сумму произойдёт сделка, если не возможна 0
    virtual int Deal(SellOrder* order) = 0;  // совершение сделки(с сохранением информаци о ней в файл


    int getMinRoomCount(){return  minRoomCount;}
    int getMaxRoomCount(){return  maxRoomCount;}
    float getMinArea(){return minArea;}
    float getMaxArea(){return maxArea;}
};

class SellOrder : virtual public Order //Класс заказа для продающей стороны
{
private:
    Apartment* apartment; //Продаваемый объект
public:
    SellOrder(QString AuthorName, int MinPrice, int MaxPrice, Apartment* apartment);
    virtual ~SellOrder(){};
    //аналогичные BuyOrder
    virtual bool isDeal(BuyOrder* order) = 0;
    virtual int DealPrice(BuyOrder* order) = 0;
    virtual int Deal(BuyOrder* order) = 0;

    int getRoomCount(){return apartment->getRoomCount();}
    float getArea(){return apartment->getArea();}
};

class RentOrder : virtual public Order //Заказ связанный с арендой/сдачей
{
private:
    int minMonth; //минимальный порог согласия по месяцам
    int maxMonth; //максимальный
public:
    RentOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinMonth, int MaxMonth);
    ~RentOrder(){};
    int getMinMonth(){return minMonth;}
    int getMaxMonth(){return maxMonth;}
};
class SaleOrder : virtual public Order //Заказ с обычной купле/продажей
{
public:
    SaleOrder(QString AuthorName, int MinPrice, int MaxPrice);
    ~SaleOrder(){};
};

/*/////////////////////////////////////////////////////////////////////////////////////////*/


class RentBuyOrder : public BuyOrder, public RentOrder // Заказ покупающей стороны, связанный с арендой/сдачей
{
private:
public://конструктор
    RentBuyOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinRoomCount, int MaxRoomCount, float MinArea, float MaxArea, int MinMonth, int MaxMonth) :
        //Конструкторы базовых классов
        Order(AuthorName, MinPrice, MaxPrice),//Первым нужно вызвать конструктор базового класс, так как следующие виртуально наследуются от него
        BuyOrder(AuthorName, MinPrice, MaxPrice, MinRoomCount, MaxRoomCount, MinArea, MaxArea),//создаётся на базе Выше созданного
        RentOrder(AuthorName, MinPrice, MaxPrice, MinMonth, MaxMonth){} //создаётся на базе Order
    ~RentBuyOrder(){};
    //перегружаются виртуальные методы
    bool isDeal(SellOrder* order);
    int DealPrice(SellOrder* order);
    int Deal(SellOrder *order);
};

class RentSellOrder : public SellOrder, public RentOrder // Заказ продающей стороны, связанный с арендой/сдачей
{
public:
    RentSellOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinMonth, int MaxMonth, Apartment* apartment) :
    Order(AuthorName, MinPrice, MaxPrice),
    SellOrder(AuthorName, MinPrice, MaxPrice, apartment),
    RentOrder(AuthorName, MinPrice, MaxPrice, MinMonth, MaxMonth){}
    ~RentSellOrder(){};
    bool isDeal(BuyOrder* order);
    int DealPrice(BuyOrder* order);
    int Deal(BuyOrder *order);
};

class SaleBuyOrder : public BuyOrder, public SaleOrder // Заказ покупающей стороны (купля/продажа)
{
public:
    SaleBuyOrder(QString AuthorName, int MinPrice, int MaxPrice, int MinRoomCount, int MaxRoomCount, float MinArea, float MaxArea) :
        Order(AuthorName, MinPrice, MaxPrice),
        BuyOrder(AuthorName, MinPrice, MaxPrice, MinRoomCount, MaxRoomCount, MinArea, MaxArea),
        SaleOrder(AuthorName, MinPrice, MaxPrice){}
    ~SaleBuyOrder(){};
    bool isDeal(SellOrder* order);
    int DealPrice(SellOrder* order);
    int Deal(SellOrder *order);
};
class SaleSellOrder : public SellOrder, public SaleOrder // Заказ продающей стороны (купля/продажа)
{
public:
    SaleSellOrder(QString AuthorName, int MinPrice, int MaxPrice, Apartment* apartment) :
        Order(AuthorName, MinPrice, MaxPrice),
        SellOrder(AuthorName, MinPrice, MaxPrice, apartment),
        SaleOrder(AuthorName, MinPrice, MaxPrice){}
    ~SaleSellOrder(){};
    bool isDeal(BuyOrder* order) override;
    int DealPrice(BuyOrder* order) override;
    int Deal(BuyOrder *order) override;
};

#endif // ORDER_H
