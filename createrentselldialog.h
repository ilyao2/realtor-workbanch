#ifndef CREATERENTSELLDIALOG_H
#define CREATERENTSELLDIALOG_H

#include <QDialog>
#include "order.h"

namespace Ui {
class CreateRentSellDialog;
}

class CreateRentSellDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateRentSellDialog(QWidget *parent = nullptr);
    ~CreateRentSellDialog();
    RentSellOrder* getLastOrder();

signals:
    void orderGenerated();
private slots:
    void on_Cancal_clicked();

    void on_create_clicked();

private:
    Ui::CreateRentSellDialog *ui;
    RentSellOrder* order;
    QList<Apartment*> apartments;

    void setDefault();
};

#endif // CREATERENTSELLDIALOG_H
