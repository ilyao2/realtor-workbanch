#ifndef CREATESALEBUYDIALOG_H
#define CREATESALEBUYDIALOG_H

#include <QDialog>
#include "order.h"

namespace Ui {
class CreateSaleBuyDialog;
}

class CreateSaleBuyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateSaleBuyDialog(QWidget *parent = nullptr);
    ~CreateSaleBuyDialog();
    SaleBuyOrder* getLastOrder();

signals:
    void orderGenerated();
private slots:
    void on_Cancal_clicked();

    void on_create_clicked();

private:
    Ui::CreateSaleBuyDialog *ui;
    SaleBuyOrder* order;

    void setDefault();
};

#endif // CREATESALEBUYDIALOG_H
